<?php

use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/', function () {
    return response()->json('Api Enable', 200);
});

Route::get("create/user", [UserController::class, 'create']);
Route::get("get/user/{id}", [UserController::class, 'get']);
Route::get("delete/user/{id}", [UserController::class, 'delete']);
