<?php

namespace App\Http\Controllers;

use App\Models\User;
use DB;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{
    public function create()
    {
        try {
            DB::beginTransaction();
            $faker = Faker::create('id_ID');
            $user = new User();
            $user->name = $faker->name;
            $user->email = $faker->email;
            $user->password = $faker->password;
            $user->save();

            Redis::set('user' . $user->id, $user);
            DB::commit();
            return response()->json(["user" => $user], 200);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function get($id)
    {
        $cached = Redis::get('user' . $id);
        if (isset($cached)) {
            $user = json_decode($cached, false);
            $from = 'cache';
        } else {
            $user = User::find($id);
            if (!$user) {
                $from = 'tidak ada';
                $user = 'User dengan id: ' . $id . ' tidak ditemukan';
            } else {
                $from = 'db';
                Redis::set('user' . $id, $user);
            }
        }
        return response()->json(["user" => $user, "from" => $from], 200);
    }

    public function delete($id)
    {
        $status = 'User tidak ditemukan';
        $user = User::find($id);
        if ($user) {
            $user->delete();
            Redis::del('user' . $id);
            $status = 'Berhasil hapus data dengan id ' . $id;
        }
        return response()->json(["status" => $status], 200);
    }
}
